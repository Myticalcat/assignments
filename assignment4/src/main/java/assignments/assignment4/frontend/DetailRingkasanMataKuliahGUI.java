package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI extends MainMenuGUI {
    private MataKuliah mataKuliah;
    private Font consolasBold = new Font("Consolas", Font.BOLD , 12);

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        super(frame, daftarMahasiswa, daftarMataKuliah);
        this.mataKuliah = mataKuliah;
        initGUI();
        
    }

    @Override
    protected void initGUI() {
        if(mataKuliah == null){
            return;
        }

        JPanel panelUtama = new JPanel(){
            // membuat background
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(panelBg.getImage(), 0, 0, this);
            }
        };
        panelUtama.setBackground(mainCol);
        panelUtama.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        // mengatur judul
        title.setText("Detail Ringkasan MataKuliah");
        title.setForeground(textCol);

        // membuat detail
        JLabel isiData = new JLabel();
        isiData.setFont(consolasBold);
        isiData.setForeground(textCol);
        isiData.setText("<html><body>Nama Mata Kuliah: " + mataKuliah.getNama() + "<br>" +
                "Kode: " + mataKuliah.getKode() + "<br>" +
                "SKS: " + mataKuliah.getSKS() + "<br>" +
                "Jumlah Mahasiswa: " + mataKuliah.getJumlahMahasiswa() + "<br>" +
                "Kapasistas: " + mataKuliah.getKapasitas() + "<br>" +
                "Daftar Mahasiswa:" + "</body></html>");

        // memprint matkul
        JLabel mhsLable = new JLabel();
        mhsLable.setFont(consolasBold);
        mhsLable.setForeground(textCol);
        String isiMhs = "<html><body>";
        int index = 0;
        for (Mahasiswa mhs : mataKuliah.getDaftarMahasiswa()) {
            if (mhs== null) break;
            index++;
            isiMhs += index +". " + mhs.getNama() + "<br>";
        }

        isiMhs += "</body></html>";

        // jka blm ambil
        if(index == 0){
            isiMhs = "Belum ada mata kuliah yang diambil.";
        }

        mhsLable.setText(isiMhs);


        //button kembali
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        stylize(kembaliButton);
        kembaliButton.addActionListener(e -> HomeGUI.setSwitcher("Home"));


        //menambahkan
        constraints.gridy = 1;
        panelUtama.add(title, constraints);
        constraints.gridy++;
        panelUtama.add(isiData ,constraints);
        constraints.gridy++;
        panelUtama.add(mhsLable ,constraints);
        constraints.gridy++;
        panelUtama.add(kembaliButton,constraints);

        panelUtama.setVisible(true);
        panel = panelUtama;
    }
}
