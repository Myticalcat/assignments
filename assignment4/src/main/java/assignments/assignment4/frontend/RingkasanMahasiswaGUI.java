package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI extends MainMenuGUI{
    private String[] npmList;
    private JComboBox<String> opsiMhs;
    private JPanel panelPilihan;
    private JPanel panelUtama;
    private DetailRingkasanMahasiswaGUI detailRingkasanMahasiswaGUI;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        super(frame, daftarMahasiswa, daftarMataKuliah);

    }

    @Override
    protected void initGUI() {
        panelUtama = new JPanel();
        panelUtama.setLayout( new BorderLayout());
        panelPilihan = createImagePanel();
        panelPilihan.setBackground(mainCol);
        panelPilihan.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);

        // mengatur judul
        title.setText("Ringkasan Mahasiswa");
        title.setForeground(textCol);

        // membuat drop down menu

        opsiMhs = new JComboBox();
        opsiMhs.setPreferredSize(comboBoxDim);
        MainMenuGUI.titledBorderinit(opsiMhs, "Pilih NPM", textCol);

        JButton lihatButton = new JButton("Lihat");
        lihatButton.setPreferredSize(buttonsDim);
        lihatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lihat();
            }
        });

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomeGUI.setSwitcher("Home");
            }
        });


        // dekorasi
        stylize(opsiMhs);
        stylize(kembaliButton);
        stylize(lihatButton);

        // adding
        constraints.gridy++;
        panelPilihan.add(title);
        constraints.gridy++;
        panelPilihan.add(opsiMhs, constraints);
        constraints.gridy++;
        panelPilihan.add(lihatButton, constraints);
        constraints.gridy++;
        panelPilihan.add(kembaliButton, constraints);
        panelUtama.add(panelPilihan, BorderLayout.CENTER);

        setPanel(panelUtama);
    }

    // merefresh pilihan apa saja yang ada
    public void refreshList(){
        if(daftarMhs != null && daftarMhs.size() != 0){

            npmList = new String[daftarMhs.size()];
            // diambil npmnya
            for (int i = 0; i < daftarMhs.size(); i++) {
                npmList[i] = String.valueOf(daftarMhs.get(i).getNpm());
            }

            // disort
            Sorter.sortItem(npmList);

            // direfresh listnya
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(npmList);
            opsiMhs.setModel(model);
        }
    }

    public void setPanelPilihan(boolean saklar) {
        this.panelPilihan.setVisible(saklar);
    }

    public void setDetail(boolean saklar) {
        if(detailRingkasanMahasiswaGUI != null){
            this.detailRingkasanMahasiswaGUI.getPanel().setVisible(saklar);
        }
    }
    private void lihat(){

        if(opsiMhs.getSelectedItem() == null){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
            return;
        }

        String npm = (String) opsiMhs.getSelectedItem();
        Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npm));

        detailRingkasanMahasiswaGUI = new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMhs, daftarMatkul);

        // switch panel
        panelUtama.add(detailRingkasanMahasiswaGUI.getPanel(), BorderLayout.CENTER);
        setPanelPilihan(false);
        setDetail(true);
    }
}
