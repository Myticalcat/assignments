package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// import date untuk tanggal skrng
import java.util.Date;
import java.text.SimpleDateFormat;


import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public static Color backColor = Color.decode("#0c1e3b");
    public static Color foreColor = Color.decode("#d0e0dd");
    private JFrame frame;
    private ArrayList<Mahasiswa> daftarMhs;
    private ArrayList<MataKuliah> daftarMatkul;
    private JPanel sideMenu;
    private static JPanel mainMenu;
    private JPanel titlePanel;
    private JPanel buttonPanel;
    private JPanel miscPanel;
    private static CardLayout switcher;
    private TambahIRSGUI tambahIRSGUI;
    private TambahMataKuliahGUI tambahMataKuliahGUI;
    private TambahMahasiswaGUI tambahMahasiswaGUI;
    private HapusIRSGUI hapusIRSGUI;
    private RingkasanMahasiswaGUI ringkasanMahasiswaGUI;
    private RingkasanMataKuliahGUI ringkasanMataKuliahGUI;
    public static String shown;


    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        this.frame = frame;
        this.daftarMhs = daftarMahasiswa;
        this.daftarMatkul = daftarMataKuliah;
        initGUI();

        
    }


    private void initGUI(){
        // mengatur frame
        frame.setLayout(new GridLayout());
        frame.setBackground(backColor);
        frame.setTitle("Administrator - Sistem Akademik");

        // membuat container
        JPanel container = new JPanel();
        container.setLayout(new BorderLayout());

        // UI utama
        // UI side menu
        sideMenu = new JPanel();
        sideMenu.setBackground(backColor);
        sideMenu.setLayout(new BorderLayout());
        sideMenu.setPreferredSize(new Dimension(290,618));

        // UI main menu
        mainMenu = new JPanel();
        mainMenu.setPreferredSize(new Dimension(700,618));
        switcher = new CardLayout();
        mainMenu.setLayout(switcher);
        mainMenuController();

        //UI untuk side menu
        titlePanel = new JPanel();
        initTitlePanel();

        // membuat Button
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        initButtonPanel(new GridBagConstraints());

        // mics panel
        miscPanel = new JPanel();
        miscPanel.setLayout(new GridBagLayout());
        initMiscPanel();

        // membuat gambar sementara untuk main menu
        JLabel imageLable = new JLabel();
        ImageIcon image = new ImageIcon("assignment4/src/main/java/assignments/assignment4/assets/cat_edit.gif");
        imageLable.setIcon(image);
        JPanel homePanel = new JPanel();
        homePanel.setLayout(new BorderLayout());
        homePanel.add(imageLable, BorderLayout.CENTER);

        // menata UI
        sideMenu.add(titlePanel, BorderLayout.NORTH);
        sideMenu.add(buttonPanel, BorderLayout.CENTER);
        sideMenu.add(miscPanel, BorderLayout.SOUTH);
        sideMenu.setVisible(true);
        mainMenu.add(homePanel, "Home");
        switcher.show(mainMenu, "Home");
        container.add(mainMenu, BorderLayout.WEST);
        container.add(sideMenu,BorderLayout.EAST);
        frame.add(container, BorderLayout.CENTER);
    }

    private void initButtonPanel(GridBagConstraints buttonConstraints){
        // button untuk button panel
        // membuat list agar mudah diaturnya
        ArrayList<JButton> buttons = new ArrayList<>();

        JButton tambahMhs = new JButton("Tambah Mahasiswa");
        buttons.add(tambahMhs);
        JButton tambahMatkul = new JButton("Tambah Mata Kuliah");
        buttons.add(tambahMatkul);
        JButton tambahIRS = new JButton("Tambah IRS");
        buttons.add(tambahIRS);
        JButton hapusIRS = new JButton("Hapus IRS");
        buttons.add(hapusIRS);
        JButton ringkasanMhs = new JButton("Lihat Ringkasan Mahasiswa");
        buttons.add(ringkasanMhs);
        JButton ringkasanMatkul = new JButton("Lihat Ringkasan Mata Kuliah");
        buttons.add(ringkasanMatkul);

        // mengatur estetika button
        buttonConstraints.gridx = 1;
        buttonConstraints.gridy = 1;
        buttonConstraints.insets = new Insets(4,4,4,4);
        for (JButton button: buttons) {
            // aksesoris
            buttonConstraints.gridy++;
            button.setForeground(foreColor);
            button.setFocusable(false);
            button.setBackground(backColor);
            button.setFont(SistemAkademikGUI.fontGeneral);
            button.setPreferredSize(new Dimension(250, 50));

            // menambahkan ke panel
            buttonPanel.add(button, buttonConstraints);
        }

        // menambahkan action listener
        tambahMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, tambahMhs.getText());
                tambahMahasiswaGUI.setKosong();
            }
        });
        tambahMatkul.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, tambahMatkul.getText());
                tambahMahasiswaGUI.setKosong();
            }
        });

        // setiap klik tambah, hapus irs, dan ringkasan akan di sort
        tambahIRS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, tambahIRS.getText());
                tambahIRSGUI.refreshList();

            }
        });
        hapusIRS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, hapusIRS.getText());
                hapusIRSGUI.refreshList();
            }
        });

        ringkasanMhs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, ringkasanMhs.getText());
                ringkasanMahasiswaGUI.refreshList();
                ringkasanMahasiswaGUI.setPanelPilihan(true);
                ringkasanMahasiswaGUI.setDetail(false);
            }
        });

        ringkasanMatkul.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switcher.show(mainMenu, ringkasanMatkul.getText());
                ringkasanMataKuliahGUI.refreshList();
                ringkasanMataKuliahGUI.setPanelPilihan(true);
                ringkasanMataKuliahGUI.setDetail(false);
            }
        });

        // membuat border
        MainMenuGUI.titledBorderinit(buttonPanel, "Menu", foreColor);

        buttonPanel.setBackground(backColor);
        buttonPanel.setVisible(true);
    }

    private void initTitlePanel(){
        // membuat title lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("<html><body>Selamat Datang" + "<br>" +
                "di" + "<br>" +
                "Sistem Akademik"+"</body></html>");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(foreColor);

        // atur title panel
        titlePanel.setBackground(backColor);
        titlePanel.setVisible(true);
        titlePanel.add(titleLabel);
    }

    private void initMiscPanel(){

        JPanel clkPan = new JPanel();
        clkPan.setBackground(backColor);

        JLabel clock = new JLabel();
        clock.setHorizontalAlignment(JLabel.CENTER);
        clock.setForeground(Color.decode("#ff6e00"));

        try {
            // menggunakan custom font
            clock.setFont( Font.createFont(Font.TRUETYPE_FONT, new File("assignment4/src/main/java/assignments/assignment4/assets/poxel-font.ttf")).deriveFont(30f));
        } catch (IOException | FontFormatException e) {
            e.printStackTrace();
        }

        // membuat date format untuk ditampilkan
        SimpleDateFormat clockFormat = new SimpleDateFormat("HH:mm:ss");
        clock.setText(clockFormat.format(new Date()));

        // akan dijalankan tiap detik
        ActionListener updateClockAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clock.setText(clockFormat.format(new Date()));
            }
        };

        // menjalankan panel
        Timer t = new Timer(1000, updateClockAction);
        t.start();

        // membuat lable credit
        // menampilkan dan megnatur misc
        miscPanel.setLayout(new BorderLayout());
        clkPan.add(clock);
        miscPanel.add(clkPan, BorderLayout.NORTH);
        miscPanel.setBackground(backColor);
        MainMenuGUI.titledBorderinit(clkPan, "Clock", foreColor);
    }

    private void mainMenuController(){
        tambahIRSGUI = new TambahIRSGUI(frame,daftarMhs, daftarMatkul);
        tambahMataKuliahGUI = new TambahMataKuliahGUI(frame,daftarMhs, daftarMatkul);
        tambahMahasiswaGUI = new TambahMahasiswaGUI(frame,daftarMhs, daftarMatkul);
        hapusIRSGUI = new HapusIRSGUI(frame,daftarMhs, daftarMatkul);
        ringkasanMahasiswaGUI = new RingkasanMahasiswaGUI(frame,daftarMhs, daftarMatkul);
        ringkasanMataKuliahGUI = new RingkasanMataKuliahGUI(frame,daftarMhs, daftarMatkul);

        mainMenu.add(tambahMahasiswaGUI.getPanel(), "Tambah Mahasiswa");
        mainMenu.add(tambahMataKuliahGUI.getPanel(), "Tambah Mata Kuliah");
        mainMenu.add(tambahIRSGUI.getPanel(), "Tambah IRS");
        mainMenu.add(hapusIRSGUI.getPanel(), "Hapus IRS");
        mainMenu.add(ringkasanMahasiswaGUI.getPanel(), "Lihat Ringkasan Mahasiswa");
        mainMenu.add(ringkasanMataKuliahGUI.getPanel(), "Lihat Ringkasan Mata Kuliah");

    }

    public static void setSwitcher(String panel){
        switcher.show(mainMenu, panel);
        shown = panel;
    }
}