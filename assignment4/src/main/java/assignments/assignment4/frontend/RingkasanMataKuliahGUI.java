package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI extends MainMenuGUI {
    private String[] namaList;
    private JComboBox opsiMatkul;
    private JPanel panelPilihan;
    private JPanel panelUtama;
    private DetailRingkasanMataKuliahGUI detailRingkasanMataKuliahGUI;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        super(frame, daftarMahasiswa, daftarMataKuliah);

    }

    @Override
    protected void initGUI() {
        panelUtama = new JPanel();
        panelUtama.setLayout( new BorderLayout());
        panelPilihan = createImagePanel();
        panelPilihan.setBackground(mainCol);
        panelPilihan.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);

        // mengatur judul
        title.setText("Ringkasan Mata Kuliah");


        // membuat drop down menu

        opsiMatkul = new JComboBox<>();
        opsiMatkul.setPreferredSize(comboBoxDim);
        MainMenuGUI.titledBorderinit(opsiMatkul, "Pilih Matkul", textCol);

        JButton lihatButton = new JButton("Lihat");
        lihatButton.setPreferredSize(buttonsDim);
        lihatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lihat();
            }
        });

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomeGUI.setSwitcher("Home");
            }
        });


        // dekorasi
        stylize(opsiMatkul);
        stylize(kembaliButton);
        stylize(lihatButton);

        // adding
        constraints.gridy++;
        panelPilihan.add(title);
        constraints.gridy++;
        panelPilihan.add(opsiMatkul, constraints);
        constraints.gridy++;
        panelPilihan.add(lihatButton, constraints);
        constraints.gridy++;
        panelPilihan.add(kembaliButton, constraints);
        panelUtama.add(panelPilihan, BorderLayout.CENTER);

        setPanel(panelUtama);
    }

    // merefresh pilihan apa saja yang ada
    public void refreshList(){
        if(daftarMatkul != null && daftarMatkul.size() != 0){

            namaList = new String[daftarMatkul.size()];
            // diambil npmnya
            for (int i = 0; i < daftarMatkul.size(); i++) {
                namaList[i] = String.valueOf(daftarMatkul.get(i).getNama());
            }

            // disort
            Sorter.sortItem(namaList);

            // direfresh listnya
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(namaList);
            opsiMatkul.setModel(model);
        }
    }

    public void setPanelPilihan(boolean saklar) {
        this.panelPilihan.setVisible(saklar);
    }

    public void setDetail(boolean saklar) {
        if (detailRingkasanMataKuliahGUI != null){
            this.detailRingkasanMataKuliahGUI.getPanel().setVisible(saklar);
        }
    }

    private void lihat(){

        if(opsiMatkul.getSelectedItem() == null){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
            return;
        }

        String nama = (String) opsiMatkul.getSelectedItem();
        MataKuliah mataKuliah = getMataKuliah(nama);

        detailRingkasanMataKuliahGUI = new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMhs, daftarMatkul);

        // switch panel
        panelUtama.add(detailRingkasanMataKuliahGUI.getPanel(), BorderLayout.CENTER);
        setPanelPilihan(false);
        setDetail(true);
    }
}
