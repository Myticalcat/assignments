package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super(nama, "Dosen");
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        if (this.mataKuliah != null){
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", getNama(), this.mataKuliah.getNama());
            return;
        }

        if(mataKuliah.getDosen() != null){
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah.getNama());
            return;
        }
        mataKuliah.addDosen(this);
        this.mataKuliah = mataKuliah;
        System.out.printf("%s mengajar mata kuliah %s\n", getNama(), mataKuliah.getNama());
    }

    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        if(mataKuliah == null){
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this.getNama());
            return;
        }

        mataKuliah.dropDosen();
        System.out.printf("%s berhenti mengajar %s\n", getNama(), this.mataKuliah.getNama());
        mataKuliah = null;
    }
}