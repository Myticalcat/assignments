package assignments.assignment3;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    // dirubah jadi static semua supaya bisa dipanggil tanpa membuat objek di main

    static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    // bisa digunakans sebagai index karena tidak ada penghapusan elemen array
    static int totalMataKuliah = 0;

    static int totalElemenFasilkom = 0;

    static ElemenFasilkom searhElement(String nama){
        for (ElemenFasilkom target: daftarElemenFasilkom) {
            if(target != null && target.getNama().equals(nama)) return target;
        }
        return null;
    }

    static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;

    }

    static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;
    }

    static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;
    }

    static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        if(objek1.equals(objek2)){
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        }

        ElemenFasilkom orang1 = searhElement(objek1);
        ElemenFasilkom orang2 = searhElement(objek2);

        if((orang1) == null || orang2 == null){
            System.out.println("Element tidak ditemukan");
            return;
        }

        if(!orang1.sudahMenyapa(orang2)){
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", objek1, objek2);
            return;
        }

        // karena 2 arah
        orang1.menyapa(orang2);
        orang2.menyapa(orang1);
        System.out.printf("%s menyapa dengan %s\n", objek1, objek2);

    }

    static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom hasilSearch = searhElement(objek);

        if(hasilSearch == null) {
            System.out.println("Element tidak ditemukan");
            return;
        }

        if(hasilSearch instanceof ElemenKantin){
            ElemenKantin elemenKantin = (ElemenKantin) hasilSearch;
            elemenKantin.setMakanan(namaMakanan, harga);
            return;
        }

        System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", hasilSearch.getNama());
    }

    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom pembeli = searhElement(objek1);
        ElemenFasilkom penjual = searhElement(objek2);

        if(pembeli == null && penjual == null){
            System.out.println("Element tidak ditemukan");
            return;
        }

        if(!(penjual instanceof ElemenKantin)){
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
            return;
        }

        // jika penjual == pembeli berarti 22nya adalah elemen kantin
        if(pembeli == penjual){
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            return;
        }

        ElemenFasilkom.membeliMakanan(pembeli, penjual, namaMakanan);

    }

    static MataKuliah searcMatkul(String nama){
        for (MataKuliah target: daftarMataKuliah) {
            if(target != null && target.getNama().equals(nama)) return target;
        }
        return null;
    }

    static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
        totalMataKuliah++;
    }

    static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom searchMahasiswa = searhElement(objek);
        MataKuliah mataKuliah = searcMatkul(namaMataKuliah);
        if(searchMahasiswa == null && mataKuliah == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        if(searchMahasiswa instanceof Mahasiswa){
            Mahasiswa mahasiswa = (Mahasiswa) searchMahasiswa;
            mahasiswa.addMatkul(mataKuliah);
            return;
        }

        System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
    }

    static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom searchMahasiswa = searhElement(objek);
        MataKuliah mataKuliah = searcMatkul(namaMataKuliah);
        if(searchMahasiswa == null && mataKuliah == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        if(searchMahasiswa instanceof Mahasiswa){
            Mahasiswa mahasiswa = (Mahasiswa) searchMahasiswa;
            mahasiswa.dropMatkul(mataKuliah);
            return;
        }

        System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom searchDosen = searhElement(objek);
        MataKuliah mataKuliah = searcMatkul(namaMataKuliah);
        if(searchDosen == null && mataKuliah == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        if(searchDosen instanceof Dosen){
            Dosen dosen = (Dosen) searchDosen;
            dosen.mengajarMataKuliah(mataKuliah);
            return;
        }

        System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
    }

    static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom searchDosen = searhElement(objek);
        if(searchDosen == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        if(searchDosen instanceof Dosen){
            Dosen dosen = (Dosen) searchDosen;
            dosen.dropMataKuliah();
            return;
        }

        System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
    }

    static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom hasilSearch = searhElement(objek);

        if (hasilSearch == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        if(hasilSearch instanceof Mahasiswa){
            Mahasiswa mahasiswa = (Mahasiswa) hasilSearch;
            mahasiswa.printData();
            return;
        }

        System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", objek);
    }

    static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah mataKuliah = searcMatkul(namaMataKuliah);

        if (mataKuliah == null){
            System.out.println("Objek tidak ditemukan");
            return;
        }

        mataKuliah.printData();
    }

    static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        // akan di reset semuanya
        for (ElemenFasilkom elemen:
                daftarElemenFasilkom) {
            if(elemen != null) {
                elemen.setFriendship(totalElemenFasilkom);
                elemen.resetMenyapa();
            }
        }

        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */
        // mensort array berdasarkan friendship kemudian nama
        ElemenFasilkom[] noNull = Arrays.copyOfRange(daftarElemenFasilkom, 0, totalElemenFasilkom);

        Arrays.sort(noNull);
        int index = 0;
        for (ElemenFasilkom elemen:
                noNull) {
                index++;
                System.out.printf("%d. %s(%d)\n", index, elemen, elemen.getFriendship());
        }
    }

    static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}