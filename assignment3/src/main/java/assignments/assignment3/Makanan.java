package assignments.assignment3;

import java.util.Objects;

class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String nama;

    long harga;

    public Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public long getHarga() {
        return harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return getNama();
    }
}